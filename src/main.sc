init: Main.preProcess()

theme: /main
    # Приветствие
    state: start
        eg!: Main.Start
        q!: *start *
        script:
            Main.init();  
        #a: Привет {{ (typeof $session.start !== "undefined" ? ", " + $session.start.name : "") }}! Я Ваш личный помощник! Чем я могу Вам помочь?
        
        if: (!$client.complete_study)
            go!: /onboarding/init
        
        if: $session.start.name
            a: Привет, {{ $session.start.name }}! Я Ваш личный помощник! Чем я могу Вам помочь?
        else:
            random:
                a: Чем я ещё могу Вам помочь? 
                a: Выберите дейстие! 
        script:
            $reactions.buttons({text: 'Поставить задачу', transition: '/task/init'});
            $reactions.buttons({text: 'Поиск по порталу', transition: '/search/init'});
            $reactions.buttons({text: 'Выбрать форму заявки', transition: '/main/choose_form'});
            $reactions.buttons({text: 'Повторить обучающий курс', transition: '/onboarding/init'});
        q: *
        if: ($request.query !== "")
            go!: /main    
    state: can_i_help
        random:
            a: Могу я вам еще чем-то помочь?
        script:
            $reactions.buttons({text: 'Поиск', transition: '/search/init'});
        
            //$reactions.buttons({text: 'Задать вопрос компании', transition: '/get_company_question'});
            $reactions.buttons({text: 'Выбрать форму заявки', transition: '/main/choose_form'});
 
    # Задать вопрос компании
    state: get_company_question
        eg!: Main.HaveAQuestion
        a: Какой вопрос Вы хотели бы задать компании?
        
        # Отпрака вопроса компании
        state: send_company_question
            q: $nonEmptyGarbage
            script:
                Forms.sendCompanyQuestion($request.query);
            go!:/can_i_help
    
    
    # Выбор группы справок
    #state: choose_app_group
    #    eg!: Main.NeedStatement
    #    a: Отлично! Для начала выберите, к какой группе относится Ваша справка.
    #    script:
    #        Object.keys($client.arAppGroups).forEach(function(element, index) {
    #            $reactions.buttons({text: $client.arAppGroups[element].NAME, transition: '/main/choose_app_type'});
    #        });
    #    
    #    state: something
    #        q: *
    #        a: Выберите, к какой группе относится Ваша справка
    #        script:
    #            Object.keys($client.arAppGroups).forEach(function(element, index) {
    #                $reactions.buttons({text: $client.arAppGroups[element].NAME, transition: '/main/choose_app_type'});
    #            });
    
     # Выбор типа справки по названию группы    
    state: choose_form
        eg!: Main.Forms
        a: Теперь давайте выберем нужную нам справку!
        script:
            Forms.getForms();
            
    # Выбор типа справки по названию группы    
    #state: choose_app_type
    #    a: Теперь давайте выберем нужную нам справку!
    #    state: something
    #        q: *
    #        a: Выберите нужную справку
    #        script:
    #            var appGroupName = $client.lastGroupName;
    #            var appTypes = Forms.getAppTypesByGroupName(appGroupName);
    #            Object.keys(appTypes).forEach(function(element, index) {
    #                $reactions.buttons({text: appTypes[element], transition: '/main/app_type_fields'});
    #            });
    #            
    #            $reactions.buttons({text: 'Перейти к группам справок', transition: '/main/choose_app_group'});
            
    # Отображаем список полей для заполнения
    state: app_type_fields
        script: 
            Forms.showFields($request);
        state: something
            q: *
            a: Выберите действие
            script:
                $reactions.buttons({text: 'Начать заполнение', transition: '/main/app_fill_process/app_fill_next_field'});
                $reactions.buttons({text: 'Вернуться к выбору заявок', transition: '/main/choose_form'});
            
        
    state: app_fill_process
        state: app_fill_next_field
            script:
                Forms.fillProcess();
                
            go: Answer
                
            state: Answer
                q: $nonEmptyGarbage
                script:
                    Forms.answer($request);
                    
                go!: ../../{{$client.temp.nextStep}}
                
        state: app_fill_confirm
            script:
                Forms.fillConfirm();
            
            state: AnswerYes
                q: Да*
                go!:../../app_fill_finish
        
            state: AnswerNo
                q: Нет*
                a: Попробуем еще раз?
        
                state: AnswerYes
                    q: Да*
                    script:
                        $client.order.fieldsToFillStep = 1;
                        $client.order.fieldsToFillSubStep = 0;
                    go!:../../../app_fill_next_field
        
                state: AnswerNo
                    q: Нет*
                    go!:/hello
                
        state: app_fill_finish
            script:
                Forms.sendApp();
            go!:/main/can_i_help
     
            
            
            