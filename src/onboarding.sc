theme: /onboarding
    state: init
        eg!: Onboarding.Init
        random:
            a:  Давайте я расскажу Вам о портале!
            a:  Предлагаю Вам пройти обучение
            a:  Ученье-свет, а неученье - тьма!
            a:  Привет, давай я расскажу тебе о портале!
        script:
            Onboarding.init();
        go!: /onboarding/next
    state: next
        script:
            Onboarding.process();
            