theme: /task
    state: init
        eg!: Task.Init
        a: Введите название задачи
        q: *
        script:
            Task.init();
        state: set
            a: Укажите время(формат dd.mm.yyyy)
            q: *
            script:
                $client.taskFields.title = $request.query;
                
                $reactions.buttons({text: 'Выйти в главное меню', transition: '/main/start'});
            state: complete
                q: *
                script:
                    $client.taskFields.deadline = $request.query;
                    
                    $client.result = Task.process($client.taskFields);
                    
                if: (!$client.result)
                    a: Ошибка создания задачи.
                    script:
                        Task.reactionButtons();
                else:
                    script:
                        Task.success();
            