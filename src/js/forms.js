var $months = ['Январь', 'Февраль', 'Март', 'Апрель','Май', 'Июнь', 'Июль', 'Август','Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

var Forms = {
    getForms: function(){
        var $injector = $jsapi.context().injector;
        var $client = $jsapi.context().client;
        var url = $injector.platform.services.api.url+"/local/ajax/applications/forms/get_forms.php";
        
        var response = $http.get(url, {
            headers: {
                "Authorization": $injector.platform.services.api.token
            }
        });
       
        if (response.isOk) {
            $client.arForms = response.data;
            
            $reactions.buttons({text: 'На главный экран', transition: '/main/start'});
            
            Object.keys($client.arForms).forEach(function(element, index) {
                $reactions.buttons({text: $client.arForms[element], transition: '/main/app_type_fields'});
            });
        }
        else {
            return false;
        }
    },
    showFields: function(request){
        var $client = $jsapi.context().client;
        var appTypeName = request.query;
        var fieldsList = 'Потребуется заполнить следующие поля:';
        var step = 1;
        
        $client.lastTypeName = request.query;
        
        
        $client.order.webFormId = this.getFormIdByAppTypeName(appTypeName);
        
        Forms.getFieldsByFormId($client.order.webFormId);
        
    
        $client.order.fieldsToFillCount = 0;
        
        Object.keys($client.arAppFields).forEach(function(element, index) {
            fieldsList += '\n'+index+'. '+$client.arAppFields[element].CAPTION;
            
            $client.order.fieldsToFill[step] = {
                "CODE"      : element,
                "CAPTION"   : $client.arAppFields[element].CAPTION,
                "STRUCTURE" : $client.arAppFields[element].STRUCTURE
            };
            step++;
            $client.order.fieldsToFillCount++;

        });
        
        $client.order.fieldsToFillStep = 1;
        $client.order.fieldsToFillSubStep = 0;
        
        $reactions.answer(fieldsList);
        $reactions.buttons({text: 'Начать заполнение', transition: '/main/app_fill_process/app_fill_next_field'});
        $reactions.buttons({text: 'Вернуться к выбору заявок', transition: '/main/choose_form'});
    
        
    },
    fillProcess: function(){
        var $client = $jsapi.context().client;
        var $currentField = $client.order.fieldsToFill[$client.order.fieldsToFillStep];
                
        //Кол-во подподлей в поле
        $client.order.fieldsToFillSubStepCount = $currentField.STRUCTURE.length;
        
        if($client.order.fieldsToFillSubStepCount == 1){
            
            $reactions.answer($currentField.CAPTION);
            
        }else{
        
            $reactions.answer($currentField.CAPTION);
            
            Object.keys($currentField.STRUCTURE).forEach(function(element, index) {
        
                $reactions.buttons({text: $currentField.STRUCTURE[element].MESSAGE, transition: '/main/app_fill_process/app_fill_next_field/Answer'});
        
            });
        }
    },
    answer: function(request){
        var $client = $jsapi.context().client;
        var $currentField = $client.order.fieldsToFill[$client.order.fieldsToFillStep];
                    
        if($client.arAppFields[$currentField.CODE].STRUCTURE.length > 1){
        
            Object.keys($client.arAppFields[$currentField.CODE].STRUCTURE).forEach(function(element, index) {
    
                if($client.arAppFields[$currentField.CODE].STRUCTURE[element]["MESSAGE"] == request.query){
                
                    $client.arAppFields[$currentField.CODE].STRUCTURE[element].ANSWER = $client.arAppFields[$currentField.CODE].STRUCTURE[element].ID;
                
                }
            });

        }else{
            
            $client.arAppFields[$currentField.CODE].STRUCTURE[$client.order.fieldsToFillSubStep].ANSWER = request.query;
        }
        
        if($client.order.fieldsToFillCount === $client.order.fieldsToFillStep 
           && $client.order.fieldsToFillSubStepCount === $client.order.fieldsToFillSubStep + 1) {
            $client.temp.nextStep = 'app_fill_confirm';
        }
        else {
            $client.temp.nextStep = 'app_fill_next_field';
            if($client.order.fieldsToFillSubStepCount === 2 && $client.order.fieldsToFillSubStep === 0) {
                $client.order.fieldsToFillSubStep = 1;
            }
            else {
                $client.order.fieldsToFillStep++;
                $client.order.fieldsToFillSubStep = 0;
            }
        }
        
    },
    fillConfirm: function(){
        var $client = $jsapi.context().client;
        var fieldsList = 'Давайте проверим, все ли верно?';
        var $currentField = $client.order.fieldsToFill[$client.order.fieldsToFillStep];
        
        
        Object.keys($client.arAppFields).forEach(function(element, index) {
                
            fieldsList += '\n'+$client.arAppFields[element].CAPTION+':';
            
            for(var $i=0; $i<Object.keys($client.arAppFields[element].STRUCTURE).length; $i++) {
            
                if(
                    $client.arAppFields[element].STRUCTURE[$i].ANSWER !== null && 
                    $client.arAppFields[element].STRUCTURE[$i].ANSWER !== undefined
                ){
                    
                    var fieldType = $client.arAppFields[element].STRUCTURE[$i].FIELD_TYPE;
                
                    switch(fieldType) {
                        case 'dropdown':
                            fieldsList += ' '+$client.arAppFields[element].STRUCTURE[$i].MESSAGE;
                        break;
                        
                        case 'checkbox':
                            fieldsList += ' '+$client.arAppFields[element].STRUCTURE[$i].MESSAGE;
                        break;
                        
                        default:
                            fieldsList += ' '+$client.arAppFields[element].STRUCTURE[$i].ANSWER;
                        break;
                    }
                }
            }
        });    
        
        $reactions.answer(fieldsList);
    },
    getFormIdByAppTypeName: function(typeName){
        var $client = $jsapi.context().client;
        var formId;
        var isFinded = false;
        Object.keys($client.arForms).some(function(element, index) {
            if($client.arForms[element] === typeName) {
                formId = element;
                isFinded = true;
                return true;
            }
        });
        return formId;
    },
    getFieldsByFormId: function(formId){//Получаем массив полей конкретной веб-формы
        var $injector = $jsapi.context().injector;
        var $client = $jsapi.context().client;
        var url = $injector.platform.services.api.url+"/local/ajax/applications/forms/get_app_fields.php?WEB_FORM_ID="+formId;
    
        var response = $http.get(url, {
            headers: {
                "Authorization": $injector.platform.services.api.token
            }
        });
            
        if (response.isOk) {
            $client.arAppFields = response.data.QUESTIONS;
            $client.arAppUserData = response.data.USER_DATA;
            $client.arAppForm = response.data.arForm;
            return true;
        }
        else {
            return false;
        }
    },
    sendApp: function(){ //Отправка заполненной заявки на справку
        var $injector = $jsapi.context().injector;
        var $client = $jsapi.context().client;
        var url = $injector.platform.services.api.url+"/local/ajax/applications/forms/get_app_fields.php?WEB_FORM_ID="+$client.order.webFormId;
        var $currentField;
        var $appForm = {};
        
        
        $appForm.sessid = $client.arAppUserData.SESSID;
        $appForm.WEB_FORM_ID = $client.order.webFormId;
        
        for(var $i=1; $i<=$client.order.fieldsToFillCount; $i++) {
            
            $currentField = $client.order.fieldsToFill[$i];
        
            for(var $j=0; $j<Object.keys($client.arAppFields[$currentField.CODE].STRUCTURE).length; $j++) {
                
                if(
                    $client.arAppFields[$currentField.CODE].STRUCTURE[$j].ANSWER !== null && 
                    $client.arAppFields[$currentField.CODE].STRUCTURE[$j].ANSWER !== undefined
                    ){
                    
                    var fieldType = $client.arAppFields[$currentField.CODE].STRUCTURE[$j].FIELD_TYPE;
                    
                    
                    switch(fieldType) {
                        case 'dropdown':
                            
                            $appForm['form_'+fieldType+'_'+$currentField.CODE] = 
                                    $client.arAppFields[$currentField.CODE].STRUCTURE[$j].ANSWER;
                           
                        break;
                        
                        case 'checkbox':
                            
                            $appForm['form_'+fieldType + '_' + $currentField.CODE + '[]'] = 
                            $client.arAppFields[$currentField.CODE].STRUCTURE[$j].ANSWER;
                        break;
                        
                        default:
                        
                            $appForm['form_'+fieldType+'_'+$client.arAppFields[$currentField.CODE].STRUCTURE[$j].ID] = 
                            $client.arAppFields[$currentField.CODE].STRUCTURE[$j].ANSWER;
                        
                        break;
                    }
                    
                    
                    /*if(
                        $client.arAppFields[$currentField.CODE].STRUCTURE[$j].FIELD_TYPE === "dropdown" ||
                        $client.arAppFields[$currentField.CODE].STRUCTURE[$j].FIELD_TYPE === "checkbox"
                    ){
            
                        if($client.arAppFields[$currentField.CODE].STRUCTURE[$j].ANSWER !== undefined){
                        
                            $appForm['form_'+$client.arAppFields[$currentField.CODE].STRUCTURE[$j].FIELD_TYPE+'_'+$currentField.CODE] = 
                                $client.arAppFields[$currentField.CODE].STRUCTURE[$j].ANSWER;
                                
                              
                        }
                    }else{
                        $appForm['form_'+$client.arAppFields[$currentField.CODE].STRUCTURE[$j].FIELD_TYPE+'_'+$client.arAppFields[$currentField.CODE].STRUCTURE[$j].ID] = 
                            $client.arAppFields[$currentField.CODE].STRUCTURE[$j].ANSWER;
                    }*/
                }
            }
        }
         
        $appForm.web_form_submit = 'Отправить';
        
        var response = $http.post(url, {
            form: $appForm,
            headers: {
                "Authorization": $injector.platform.services.api.token
            }
        });
        
        if (response.isOk) {
            $reactions.answer('Ваша заявка отправлена! Номер заявки '
                +'<a href="'+$injector.platform.services.api.url+'/services/requests/form_edit.php?RESULT_ID='+response.data.RESULT_ID+'" target="_blank">'
                +response.data.RESULT_ID+'</a>'
                +'\nСписок своих заявок Вы можете посмотреть по '
                +'<a href="'+$injector.platform.services.api.url+'/services/requests/my.php" target="_blank">ссылке</a>');
            return true;
        }
        else {
            $reactions.answer('Ошибка!');
            return false;
        }
    },
    sendCompanyQuestion: function(message){
        var $injector = $jsapi.context().injector;
        var $client = $jsapi.context().client;
        var url = $injector.platform.services.api.url+"/local/ajax/jivoform/chat_bot.php";
        
        var response = $http.post(url, {
            body: {
                "message": message
            },
            headers: {
                "Authorization": $injector.platform.services.api.token
            }
        });
        
        if (response.isOk && response.data.code == "SUCCESS") {
            $reactions.answer('Ваш вопрос компании успешно отправлен!');
            return true;
        }
        else {
            $reactions.answer('Ошибка!');
            return false;
        }
    }
}