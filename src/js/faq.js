var Faq = {
    process: function(query){
        
        var $injector = $jsapi.context().injector;
        var $client = $jsapi.context().client;
        var url = $injector.platform.services.api.url+"/local/ajax/applications/faq/get_docs.php";
        
        var response = $http.post(url, {
            form: {
                query   : query,
            },
            headers: {
                "Authorization": $injector.platform.services.api.token
            }
        });
    
        $reactions.answer(response.data);
    }
}