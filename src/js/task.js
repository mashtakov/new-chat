var Task = {
    init: function(start){
        var $client = $jsapi.context().client;
        
        $client.taskFields = {};

        $client.taskFields.deadline         = '';
        $client.taskFields.title            = '';
        
        $reactions.buttons({text: 'Выйти в главное меню', transition: '/main/start'});
    },
    reactionButtons: function(){
        $reactions.buttons({text: 'Добавить ещё одну задачу', transition: '/task/init'});
        $reactions.buttons({text: 'Выйти в главное меню', transition: '/main/start'});
    },
    success: function(){
        var $client = $jsapi.context().client;
        var $injector = $jsapi.context().injector;
        var userId = $client.user_id;
        
        var url = $injector.platform.services.api.url;
                        
        url += '/company/personal/user/' + userId + '/tasks/task/view/' + $client.result + '/';
        
        var taskResult = 'Задача добавлена, <a href="' + url + '">открыть</a>';
        
        $reactions.answer(taskResult);
        
        this.reactionButtons();
    },
    process: function(taskFields){
        var $injector = $jsapi.context().injector;
        var $client = $jsapi.context().client;
        
        var url = $injector.platform.services.api.url+"/local/ajax/applications/task/set.php";
       
        var userId = $client.user_id;
       
       if(!this.checkDate(taskFields.deadline)){
       
            $reactions.answer('Неверный формат даты!');  
            
            return false;
             
       }else{
           //$reactions.answer("!!!ID: " + $client.user_id);
           var response = $http.post(url, {
                    form: {
                        title           : taskFields.title,
                        responsible_id  : userId,
                        deadline        : taskFields.deadline,
                    },
                    headers: {
                        "Authorization": $injector.platform.services.api.token
                    }
                }
            );
        
            if (response.isOk) {
                return response.data.id;
            }
            else {
                return false;
            }
       }    
    },
    checkDate: function(dateString){
        
        var regEx = /^\d{2}\.\d{2}\.\d{4}$/;
        if(!dateString.match(regEx)){
            return false;
        }
        
        return true;
    }
    
}