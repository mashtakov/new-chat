var Search = {
    reactionButtons: function(){
        $reactions.buttons({text: 'Задать вопрос', transition: '/search/init'});
        $reactions.buttons({text: 'Выйти в главное меню', transition: '/main/start'});
    },
    process: function(query){
        
        var $injector = $jsapi.context().injector;
        var $client = $jsapi.context().client;
        var url = $injector.platform.services.api.url+"/local/ajax/applications/search/get_docs.php";
        
        var response = $http.post(url, {
            form: {
                query   : query,
            },
            headers: {
                "Authorization": $injector.platform.services.api.token
            }
        });
        
        if(response.data === ""){
            return false;
        }
        
        return response.data;
    },
    result: function(){
        var searchResult = '';
        var $injector = $jsapi.context().injector;
        var $client = $jsapi.context().client;
        var url = $injector.platform.services.api.url;
        var i = 1;
    
        if(typeof $client.searchData === 'object'){
            Object.keys($client.searchData).forEach(function(element, index) {
                
                searchResult += $client.searchData[element].TITLE + ' - <a href="'+url+$client.searchData[element].URL+'">Подробнее</a>\n'+i+'.';
                
                i++;
            });
        }
    
        $reactions.answer(searchResult);
        
        this.reactionButtons();
    }
    
}