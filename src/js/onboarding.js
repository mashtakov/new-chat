var Onboarding = {
    init: function(){
        var $client = $jsapi.context().client;
        
        $client.onboarding = {
            data: this.getData(),
            pointer: 0
        };
    },
    reactionButtons: function(){
        $reactions.buttons({text: 'Выйти в главное меню', transition: '/main/start'});
        $reactions.buttons({text: 'Продолжить', transition: '/onboarding/next'});
    },
    getData: function(){
        var $injector = $jsapi.context().injector;
        var $client = $jsapi.context().client;
        var url = $injector.platform.services.api.url+'/local/ajax/applications/onboarding/index.php';
       
        var response = $http.get(url, {
            headers: {
                "Authorization": $injector.platform.services.api.token
            }
        });
        
        return response.data;
    },
    process: function(){
        var $injector = $jsapi.context().injector;
        var $client = $jsapi.context().client;
        var txt = '';
        
        if(typeof $client.onboarding.data[ $client.onboarding.pointer ] === "object"){
            
            txt = $client.onboarding.data[ $client.onboarding.pointer ].NAME + '\n';
            txt += $client.onboarding.data[ $client.onboarding.pointer ].DETAIL_TEXT;
            
            $reactions.answer(txt);
            $reactions.buttons({text: 'Продолжить', transition: '/onboarding/next'});    
            
            $client.onboarding.pointer++;
        }else{
            
            $client.complete_study = true;
            
            this.setComplete();
            
            txt = 'Вы прошли обучение';
            
            $reactions.answer(txt);
            $reactions.buttons({text: 'Выйти в главное меню', transition: '/main/start'});
        }
    },
    setComplete: function(){
        var $injector = $jsapi.context().injector;
        var $client = $jsapi.context().client;
        var url = $injector.platform.services.api.url+'/local/ajax/applications/onboarding/set_complete.php';
        var userId = $client.user_id;
        
        var response = $http.post(url, {
            form: {
                userId  : userId
            },
            headers: {
                "Authorization": $injector.platform.services.api.token
            }
        });
        
    }
}