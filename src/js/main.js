var Main = {
    preProcess: function(){
            
        bind("preProcess", function(ctx){
            
            var $session = ctx.session;
            var $parseTree = ctx.parseTree;
            var $client = ctx.client;

            if(Object.keys($parseTree).length !== 0){
                
                $session.start = $parseTree.text.substring($parseTree.text.lastIndexOf("start") + "start ".length);
                
                if($session.start) {
                    try {
                        $session.start = JSON.parse($session.start);
                        
                        if(typeof $session.start === "object"){
                            
                            $client.user_id = $session.start.user_id;
                            $client.complete_study = $session.start.complete_study;
                        }
                        
                    }  catch(e) {
                        
                        log("start data non JSON object");
                    }
                }
            }
        });
        
    },
    init: function(){
        var $client = $jsapi.context().client;
       
        $client.order = {};
        $client.order.fieldsToFill = {};
        $client.order.answers = {};
        $client.temp = {};
        $client.temp.nextStep = '';
    }
}