require: js/main.js
require: js/task.js
require: js/forms.js
require: js/search.js
require: js/onboarding.js
require: js/faq.js

require: scenarios/misc.sc
require: scenarios/offtopic/offtopic.sc

require: main.sc
require: search.sc
require: onboarding.sc
require: task.sc
require: faq.sc

require: patterns.sc

    module = sys.zb-common
patterns:
    $notGarbage = $ner<$nonEmptyGarbage>