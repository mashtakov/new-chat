theme: /search
    state: init
        eg!: Search.Init
        random:
            a: Какой вопрос Вас интересует?
            a: Что бы вы хотели найти?
            a: Задайте вопрос, я постараюсь ответить!
            a: Ищите документ? Я постараюсь помочь
            a: Ищите человека? Просто введите его имя
        script:
            Search.reactionButtons();
    state: process
        q: $nonEmptyGarbage
        script:
        
            $client.searchData = Search.process($request.query);

        if: (!$client.searchData)
            random:
                a: К сожалению, мне не удалось найти ответ на Ваш вопрос
                a: Мне не удалось найти ответ, скорректируйте вопрос
                a: Попробуйте скорректировать Ваш вопрос
                a: Попробуйте изменить Ваш вопрос
            script:
                Search.reactionButtons();
        else:
            random:
                a: Вот, что мне удалось найти.
                a: Результаты поиска.
                a: Результаты поиска. Хотите повторить запрос?
                a: Вот такой ответ!
                a: Мне есть о чем Вам рассказать!
            script:
                Search.result();
                