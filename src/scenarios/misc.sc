theme: /Misc

    state: Hello
        eg!: Misc.Hello
        a: {{$temp.answer_from_classifier}}

    #state: About
    #    eg!: Misc.About
    #    a: {{$temp.answer_from_classifier}}
    
    state: HowAreYou
        eg!: Misc.HowAreYou
        a: {{$temp.answer_from_classifier}}
        
    state: WhatAreYouDoing
        eg!: Misc.WhatAreYouDoing
        a: {{$temp.answer_from_classifier}}
        
    state: ThankYou
        eg!: Misc.ThankYou
        a: {{$temp.answer_from_classifier}}
        
    state: Compliment
        q!: * $compliment *
        a: Спасибо! Очень приятно такое слышать😌